package com.mofum.scope.common.annotation.metadata.service;

/**
 * 表格范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class MTableScope {

    /**
     * 表名
     */
    private String name;

    /**
     * 表别名
     */
    private String alias;

    /**
     * 表列
     */
    private MColumnScope[] columns;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public MColumnScope[] getColumns() {
        return columns;
    }

    public void setColumns(MColumnScope[] columns) {
        this.columns = columns;
    }
}
