package com.mofum.scope.common.annotation.metadata.service;

/**
 * 列范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class MColumnScope {

    /**
     * 列名
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 表名称
     */
    private String tableName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
