package com.mofum.scope.common.annotation.metadata.controller;

/**
 * 业务列
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-27
 **/
public class MServiceColumn {

    /**
     * 列名
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 方法名
     */
    private String method;

    /**
     * 是否是数组
     *
     * @return
     */
    private boolean array;

    /**
     * 分割正则表达式
     *
     * @return
     */
    private String splitRegex;

    /**
     * 是否是JSON
     */
    private boolean json;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isArray() {
        return array;
    }

    public void setArray(boolean array) {
        this.array = array;
    }

    public String getSplitRegex() {
        return splitRegex;
    }

    public void setSplitRegex(String splitRegex) {
        this.splitRegex = splitRegex;
    }

    public boolean isJson() {
        return json;
    }

    public void setJson(boolean json) {
        this.json = json;
    }
}
