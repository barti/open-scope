package com.mofum.scope.common;

import com.mofum.scope.common.model.Scope;

import java.util.List;

/**
 * 范围提取器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public interface IScopeExtractor<Params extends Object, ServiceException extends Exception> {

    /**
     * 根据参数提取范围列表
     *
     * @param params 参数
     * @return
     * @throws ServiceException 业务异常
     */
    List<? extends Scope> extractorScopes(Params params) throws ServiceException;

}