package com.mofum.scope.spring.web;

import com.mofum.scope.common.error.DefaultThrowableHandler;
import com.mofum.scope.common.error.ThrowableHandler;
import org.springframework.context.support.ApplicationObjectSupport;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-27
 **/
public abstract class AbstractAspect extends ApplicationObjectSupport {

    private ThrowableHandler throwableHandler;

    public abstract void config();

    public ThrowableHandler getThrowableHandler() {
        if (throwableHandler == null) {
            throwableHandler = new DefaultThrowableHandler();
        }
        return throwableHandler;
    }

    public void setThrowableHandler(ThrowableHandler throwableHandler) {
        this.throwableHandler = throwableHandler;
    }
}
