package com.mofum.scope.spring.boot.annotation;

import com.mofum.scope.spring.boot.registrar.DataBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启 Mybatis
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({DataBeanDefinitionRegistrar.class})
@EnableDataScope(basePackages = {
        "com.mofum.scope.spring.boot.mybatis"
})
public @interface EnableMybatisScope {
}