package com.mofum.scope.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自动配置Mybatis权限范围属性
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
@ConfigurationProperties(prefix = "mofum.mybatis.scope")
public class AutoMybatisScopeProperties {

    /**
     * 需要Mybatis
     */
    private boolean needMybatis = true;

    public boolean isNeedMybatis() {
        return needMybatis;
    }

    public void setNeedMybatis(boolean needMybatis) {
        this.needMybatis = needMybatis;
    }
}
